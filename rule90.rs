#![recursion_limit="1024"]

macro_rules! rule90 {
    // Initialization
    ( ($($fuel:tt)*) | $($cells:tt,)* ) => {
        rule90!( __cont ($($fuel)*) | ($($cells,)*) | (($($cells,)*)) );
    };

    // Initialize the line
    ( __cont (f $($fuel:tt)*) | ($($cells:tt,)*) | ($(($($history:tt,)*))* ) ) => {
        rule90!( __inline ($($fuel)*) | (.,) ($($cells,)*) | ( $(($($history,)*))* ) );
    };

    // The rule itself
    ( __inline ($($fuel:tt)*) | ($($lcells:tt,)*) (., $mid:tt, ., $($rcells:tt,)*) | ($(($($history:tt,)*))* ) ) => {
        rule90!( __inline ($($fuel)*) | ($($lcells,)* ., ) ($mid, ., $($rcells,)*) | ($(($($history,)*))* ) );
    };
    ( __inline ($($fuel:tt)*) | ($($lcells:tt,)*) (W, $mid:tt, ., $($rcells:tt,)*) | ($(($($history:tt,)*))* ) ) => {
        rule90!( __inline ($($fuel)*) | ($($lcells,)* W, ) ($mid, ., $($rcells,)*) | ($(($($history,)*))* ) );
    };
    ( __inline ($($fuel:tt)*) | ($($lcells:tt,)*) (., $mid:tt, W, $($rcells:tt,)*) | ($(($($history:tt,)*))* ) ) => {
        rule90!( __inline ($($fuel)*) | ($($lcells,)* W, ) ($mid, W, $($rcells,)*) | ($(($($history,)*))* ) );
    };
    ( __inline ($($fuel:tt)*) | ($($lcells:tt,)*) (W, $mid:tt, W, $($rcells:tt,)*) | ($(($($history:tt,)*))* ) ) => {
        rule90!( __inline ($($fuel)*) | ($($lcells,)* ., ) ($mid, W, $($rcells,)*) | ($(($($history,)*))* ) );
    };

    // Ending the line
    ( __inline ($($fuel:tt)*) | ($($lcells:tt,)*) (., .,) | ($(($($history:tt,)*))* ) ) => {
        rule90!( __inline ($($fuel)*) | ($($lcells,)* ., ) () | ($(($($history,)*))* ) );
    };
    ( __inline ($($fuel:tt)*) | ($($lcells:tt,)*) (W, .,) | ($(($($history:tt,)*))* ) ) => {
        rule90!( __inline ($($fuel)*) | ($($lcells,)* W, ) () | ($(($($history,)*))* ) );
    };
    ( __inline ($($fuel:tt)*) | ($($lcells:tt,)*) (., W,) | ($(($($history:tt,)*))* ) ) => {
        rule90!( __inline ($($fuel)*) | ($($lcells,)* ., ) () | ($(($($history,)*))* ) );
    };
//    ( __inline ($($fuel:tt)*) | ($($lcells:tt,)*) (W, .,) | ($(($($history:tt,)*))* ) ) => {
//        rule90!( __inline ($($fuel)*) | ($($lcells,)* W, ) () | ($(($($history,)*))* ) );
//    };
    ( __inline ($($fuel:tt)*) | ($($lcells:tt,)*) () | ($(($($history:tt,)*))* ) ) => {
        rule90!( __cont ($($fuel)*) | ($($lcells,)*) | ( ($($lcells,)*) $(($($history,)*))* ) );
    };

    // End of execution
    ( __cont () | ($($cells:tt,)*) | ( $( ( $($cell:tt,)* ) )*) ) => {
        compile_error!(concat!("\n", $($(stringify!($cell),)* "\n", )*));
    };
}

rule90!((f f f f f f f f f f f f f f f f f ) | W, ., ., ., ., ., ., ., ., ., ., ., ., ., ., ., ., ., ., ., ., ., ., .,);
